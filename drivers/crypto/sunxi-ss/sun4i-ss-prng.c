#include "sun4i-ss.h"

int sun4i_ss_prng_seed(struct crypto_rng *tfm, const u8 *seed,
		       unsigned int slen)
{
	struct sun4i_ss_alg_template *algt;
	struct rng_alg *alg = crypto_rng_alg(tfm);

	algt = container_of(alg, struct sun4i_ss_alg_template, alg.rng);
	memcpy(algt->ss->seed, seed, slen);

	return 0;
}

static size_t _sun4i_ss_prng_read(struct sun4i_ss_ctx *ss, u32 *data, size_t dlen)
{
	const u32 mode = SS_OP_PRNG | SS_PRNG_CONTINUE | SS_ENABLED;
	unsigned int todo;

	todo = (dlen / 4) * 4;
	dlen = todo;

	while (todo > 0) {
		size_t len;
		int i;
		u32 v;

		spin_lock_bh(&ss->slock);

		writel(mode, ss->base + SS_CTL);

		/* write the seed */
		for (i = 0; i < SS_SEED_LEN / BITS_PER_LONG; i++)
			writel(ss->seed[i], ss->base + SS_KEY0 + i * 4);

		/* Read the random data */
		len = min_t(size_t, SS_DATA_LEN / BITS_PER_BYTE, todo);
		readsl(ss->base + SS_TXFIFO, data, len / 4);
		data += len / 4;
		todo -= len;

		/* Update the seed */
		for (i = 0; i < SS_SEED_LEN / BITS_PER_LONG; i++) {
			v = readl(ss->base + SS_KEY0 + i * 4);
			ss->seed[i] = v;
		}

		spin_unlock_bh(&ss->slock);
	}

	if (dlen > 0) {
		spin_lock_bh(&ss->slock);
		writel(0, ss->base + SS_CTL);
		spin_unlock_bh(&ss->slock);
	}

	return dlen;
}

void sun4i_ss_prng_read(struct sun4i_ss_ctx *ss, void *dst, size_t dlen)
{
	size_t		len;

	/* ensure that 'dst' is aligned properly */
	if (((uintptr_t)dst % 4) != 0) {
		uint32_t	tmp;

		len = 4 - ((uintptr_t)dst % 4);
		len = min(len, dlen);

		_sun4i_ss_prng_read(ss, tmp, sizeof tmp);

		memcpy(dst, &tmp, len);
		dst  += len;
		dlen -= len;
	}

	/* read full words */
	len   = _sun4i_ss_prng_read(ss, dst, dlen);
	dst  += len;
	dlen -= len;

	/* read trailing bytes */
	if (dlen > 0) {
		uint32_t	tmp;

		_sun4i_ss_prng_read(ss, tmp, sizeof tmp);

		memcpy(dst, &tmp, dlen);
	}
}

int sun4i_ss_prng_generate(struct crypto_rng *tfm, const u8 *src,
			   unsigned int slen, u8 *dst, unsigned int dlen)
{
	struct sun4i_ss_alg_template *algt;
	struct rng_alg *alg = crypto_rng_alg(tfm);
	struct sun4i_ss_ctx *ss;

	algt = container_of(alg, struct sun4i_ss_alg_template, alg.rng);
	ss = algt->ss;

	sun4i_ss_prng_read(ss, dst, dlen);

	return 0;
}
